const serverIp = '127.0.0.1:8000'
const baseUrl=`http://${serverIp}/api/`


export async function get(url){
    const headers={
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        // 'Authorization': token
    }
    const options={
        method:"GET",
        headers:headers,
    }
    return fetch((baseUrl+url), options).then(res=> res.status===200 ? res.json() : '', err=>err);
}

export async function post(url,data){
    const headers={
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        // 'Authorization': token
    }
    const options={
        method:"POST",
        headers:headers,
        body:JSON.stringify(data),
    }
    return fetch(baseUrl+url, options).then(res=> res.json(), err=>err);
}

export async function put (url,data) {
    const headers={
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        // 'Authorization': token
    }
    const options={
        method:"PUT",
        headers:headers,
        body:JSON.stringify(data),
    }
    return fetch(baseUrl+url, options).then(res=>res.json(),err=>err);
}

export async function patch(url,data){
    const headers={
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        // 'Authorization': token
    }
    const options={
        method:"PATCH",
        headers:headers,
        body:JSON.stringify(data),
    }
    return fetch(baseUrl+url, options).then(res=>res.json(),err=>err);
}

export async function deLete(url){
    const headers={
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        // 'Authorization': token
    }
    const options={
        method:"DELETE",
        headers:headers,
    }
    return fetch((baseUrl+url), options).then(res=> res.status, err=>err);
}