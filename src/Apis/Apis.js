import { get, post, put, patch, deLete } from './ApiMethods.js';


export const getForms = async () => {
    const res = await get(`forms/all`)
    return res.length ? res : []
}

export const createForm = async (formData) => {
    const res = await post(`forms/all/`, formData)
    return res.id ? true : false
}

export const updateForm = async (formId, formData) => {
    const res = await put(`forms/all/${formId}/`, formData)
    return res 
}

export const deleteForm = async (formId) => {
    const res = await deLete(`forms/all/${formId}`)
    return res===204 ? true : false 
}