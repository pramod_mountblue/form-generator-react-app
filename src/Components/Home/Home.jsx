import React, { useState, useEffect } from 'react'
import './Home.css'
import Tabs from "../Common/Tabs/Tabs";
import Modal from '../Common/Modal/Modal'
import Input from '../Common/Input/Input/InputBox'
import Question from './Question'
import { getForms, updateForm, createForm, deleteForm } from '../../Apis/Apis'
import editIcon from '../../Images/edit.png'
import deleteIcon from '../../Images/delete.png'
const defalutFormData = { title: '', data: [] }


const Home = () => {
  const [formData, setFormData] = useState(defalutFormData)
  const [activeTab, setActiveTab] = useState({ value: '', id: null })
  const [modal, setModal] = useState({ show: false, modalOf: '', data: null })
  const [allFormsData, setAllFormsData ] = useState([])
  const [formsDataChanged, setFormDataChanged] = useState(false)


  useEffect(() => {
      (async () => {
        const forms = await getForms()
        setAllFormsData(forms)
      })()
  }, [formsDataChanged]);


  const setAcTiveTab = (tabName, tabId) => setActiveTab({ value: tabName, id: tabId })

  const toggleModal = (show, modalOf, questionId) => {
    if (questionId) {
      if (modalOf==='question') {
        const activePage = formData.data.filter(page => page.id===activeTab.id)[0]
        const question = activePage.questions.filter(question => question.id===questionId)[0]
        setModal({ show, modalOf, data: question })
      }
    } else {
      setModal({ show, modalOf })
    }
  }

  const getPages = () => formData.data.map(pageData => ({ value: pageData.page_title, id: pageData.id }))

  const handlePages = (action, pageTitle, pageId) => {
    if (action==='add') {
      const newPageId = formData.data.length ? formData.data.slice(-1)[0].id + 1 : 1
      setFormData({ ...formData, data: [...formData.data, { page_title: pageTitle, id: newPageId }]})
      setAcTiveTab(pageTitle, newPageId)
    } else if (action==='delete') {
      const pages = formData.data.filter(page => page.id !== pageId)
      setFormData({ ...formData, data: pages })
      if (pages.length) {
        const lastPage = pages[pages.length-1]
        setAcTiveTab(lastPage.page_title, lastPage.id)
      }
    }
  }

  const handleQuestions = (question, action, pageId) => {
    if (action==='add' || (action==='edit' && activeTab.id!==pageId)) {
      const page = formData.data.filter(page => page.id===pageId)[0]
      const newQuestionId = page.questions && page.questions.length ? page.questions.slice(-1)[0].id + 1 : 1
      const newQuestion = { ...question, id: newQuestionId }
      if (action==='edit' && activeTab.id!==pageId) {
          const activePage = formData.data.filter(page => page.id===activeTab.id)[0]
          const questions = activePage.questions.filter(quesTion => quesTion.id!==question.id)
          const editedPage = { ...activePage, questions }
          const allPages = formData.data.map(page => {
            if (page.id===pageId) {
              page = { ...page, questions: [ ...page.questions || [], newQuestion ]}
            } else if (page.id===editedPage.id) {
              page = editedPage
            }
            return page
          })
          setFormData({ ...formData, data: allPages })
      } else {
          const allPages = formData.data.map(page => {
            if (page.id===pageId) {
              page = { ...page, questions: [ ...page.questions || [], newQuestion ]}
            }
            return page
          })
          setFormData({ ...formData, data: allPages })
      }
      if (pageId!==activeTab.id) setActiveTab({ ...activeTab, id: pageId })
    } else if (action==='edit' && activeTab.id===pageId) {
        const page = formData.data.filter(page => page.id === pageId)[0]
        const questions = page.questions.map(quesTion => {
          if (quesTion.id===question.id) {
            quesTion = question
          }
          return quesTion
        })
        const editedPage = { ...page, questions }
        const pages = formData.data.map(page => {
          if (page.id===editedPage.id) {
            page = editedPage
          }
          return page
        })
        setFormData({ ...formData, data: pages })
    } if (action==='delete') {
        const activePage = formData.data.filter(page => page.id===activeTab.id)[0]
        const questions = activePage.questions.filter(quesTion => quesTion.id!==question.id)
        const editedPage = { ...activePage, questions }
        const pages = formData.data.map(page => {
          if (page.id===editedPage.id) {
            page = editedPage
          }
          return page
        })
        setFormData({ ...formData, data: pages })
    }
  }

  const moveQuestion = (action, questionIndex) => {
    const activePage = formData.data.filter(page => page.id===activeTab.id)[0]
    const questions = activePage.questions
    if (action==='up' && questionIndex > 0) {
      [ questions[questionIndex], questions[questionIndex-1] ] = [ questions[questionIndex-1], questions[questionIndex] ]
    } else if (action==='down' && questionIndex < questions.length-1) {
      [ questions[questionIndex], questions[questionIndex+1] ] = [ questions[questionIndex+1], questions[questionIndex] ]
    }
    const editedPage = { ...activePage, questions }
    const pages = formData.data.map(page => {
      if (page.id===editedPage.id) {
        page = editedPage
      }
      return page
    })
    setFormData({ ...formData, data: pages })
  }

  const setFormTitle = (title) => setFormData({ ...formData, title })

  const createNewForm = () => setFormData(defalutFormData)

  const editForm = (formId) => {
    const form = allFormsData.filter(form => form.id===formId)[0]
    setFormData(form)
    if (form.data && form.data.length) setAcTiveTab(form.data[0].page_title, form.data[0].id)
  }

  const deleTeForm = async(formId) => {
    await deleteForm(formId)
    setFormDataChanged(boolean => !boolean)
  }

  const saveForm = async() => {
    if (formData.id || formData.title) {
      if (formData.id) await updateForm(formData.id, formData)
      else if (formData.title) await createForm(formData)
      setFormData(defalutFormData)
      setFormDataChanged(boolean => !boolean)
    }
  }

  const questions = () => {
      const activePage = formData.data.filter(page => page.id===activeTab.id)[0] || {}
      return  activePage.questions && activePage.questions.map((question, index) => (
                <Question question={question} key={index} index={index} toggleModal={toggleModal} 
                    handleQuestions={handleQuestions} moveQuestion={moveQuestion}
                />
              ))
  }

  const form= () => (
    <div className='form'>
      <div className='form-header'>
          <Input label={'Form Title'} type={'text'} value={formData.title} onChange={setFormTitle}/>
          <button className='button' onClick={()=>toggleModal(true, 'page')}>Add New Page</button>
          <button className='button' onClick={()=>toggleModal(true, 'question')}>Add New Question</button>
          <button className='button save' onClick={saveForm}>Save</button>
      </div>
      <div>
        <Tabs tabElements={getPages()} setActiveTab={setAcTiveTab} activeTabId={activeTab.id} closeTab={handlePages} />
        <div className='questions'>
            {questions()}
        </div>
      </div>
    </div>

  )

  const allForms = () => allFormsData.map((form, index) => (
    <div className='flex alignItemsCenter justifyContentSpaceBetween forms-item' key={index}>
      <div>{form.title}</div>
      <div className='icons-container'>
          <img src={editIcon} className='action-icon' onClick={()=>editForm(form.id)}/>
          <img src={deleteIcon} className='action-icon' onClick={()=>deleTeForm(form.id)} />
      </div>
    </div>
  ))

  const forms = () => (
    <div className='forms-container'>
      {form()}
      <div className='forms'>
        <div className='flex alignItemsCenter justifyContentSpaceBetween marginBottom-2rem'>
          <div>Forms</div>
          <div className='makeButton makeBondiBlueButton' onClick={createNewForm}>Create New Form</div>
        </div>
        {allForms()}
      </div>
    </div>
  )
  

  return ( 
    <div className='container'>
        {forms()}
        {modal.show && 
          <Modal modalOf={modal.modalOf} toggleModal={toggleModal} addPage={handlePages} allPages={getPages()}
              currentPageId={activeTab.id} addQuestion={handleQuestions} data={modal.data}
          />
        }
    </div>
   );
}


export default Home