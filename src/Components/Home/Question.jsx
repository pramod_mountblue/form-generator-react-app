import React from 'react';
import './Question.css'
import editIcon from '../../Images/edit.png'
import deleteIcon from '../../Images/delete.png'
import arrowUpIcon from '../../Images/up-arrow.png'
import arrowDownIcon from '../../Images/down-arrow.png'


const Question = (props) => {
    const { question, index, toggleModal, handleQuestions, moveQuestion } = props

    const quesTion = () => (
        <div className='question-container'>
            <div className='flex alignItemsCenter justifyContentSpaceBetween'>
                <div className='marginBottom-point5rem'>{index+1}. {question.title}</div>
                <div className='icons-container'>
                    <img src={editIcon} className='action-icon' onClick={()=>toggleModal(true, 'question', question.id)}/>
                    <img src={deleteIcon} className='action-icon' onClick={()=>handleQuestions(question, 'delete')} />
                    <img src={arrowUpIcon} className='action-icon' onClick={()=>moveQuestion('up', index)}/>
                    <img src={arrowDownIcon} className='action-icon' onClick={()=>moveQuestion('down', index)}/>
                </div>
            </div>
            {options(question.options, question.optionType)}
        </div>
    )

    const options = (options, optionType) => {
        if (optionType!=='text') return options.map((option, index) => checkInput(option.value, optionType, index))
        else return textInput()
    }

    const checkInput = (label, inputType, index) => (
        <div key={index} className='marginLeft-1rem'>
            <input type={inputType} id={index} />
            <label htmlFor={index}>{label}</label>
        </div>
    )

    const textInput = () => <div className='marginLeft-1rem'><input type={'text'}/></div>


    return ( 
        <div>
            {quesTion()}
        </div>
     );
}
 
export default Question;