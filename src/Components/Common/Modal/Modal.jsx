import React, { useState, useEffect } from 'react';
import './Modal.css'
import Input from '../Input/Input/InputBox'
import MultiSelect from '../Input/MultiSelect/MultiSelect'
import closeIcon from '../../../Images/close.png'


const Modal = (props) => {
    const { modalOf, toggleModal, addPage, addQuestion, allPages, currentPageId, data } = props
    const [ pageData, setPageData ] = useState({ label: 'Page Title', type: 'text', value: '' })
    const [ questionData, setQuestionData ] = useState({ label: 'Question Title', title: '', options: [] })
    const [optionType, setOptionType] = useState({ 
        label: 'Select Option Type', type: 'select', value: 'text', id: 1, 
        options: [{ id: 1, value: 'text'}, { id: 2, value: 'radio'}, { id: 3, value: 'checkbox'}] 
    })
    const [pages, setPages] = useState({ label: 'Select Page', type: 'select', value: '', id: currentPageId, options: allPages })

    useEffect(() => {
        if (modalOf==='question' && data) {
            const question = data
            setQuestionData({ ...questionData, title: question.title, options: question.options, id: question.id })
            const optionId = question.optionType==='text' ? 1 : question.optionType==='radio' ? 2 : 3
            setOptionType({ ...optionType, value: question.optionType, id: optionId })
        }
    }, [])

    const toggLeModal = () => toggleModal(false)

    const setPageDaTa = (value) => setPageData({ ...pageData, value })

    const setQuesTionTitle = (value) => setQuestionData({ ...questionData, title: value })

    const savePage = () => addPage('add', pageData.value)

    const saveQuestion = () => {
        const { title, options, id } = questionData
        const allOptions = optionType.value==='text' ? [] : options
        const question = { id, title, optionType: optionType.value, options: allOptions }
        const action = data ? 'edit' : 'add'
        addQuestion(question, action, pages.id)
    }

    const addOption = () => {
        const newQuestionId = questionData.options.length ? questionData.options.slice(-1)[0].id + 1 : 1
        setQuestionData({ ...questionData, options: [ ...questionData.options, { id: newQuestionId, value: '' }] })
    }

    const deleteOption = (optionId) => {
        const options = questionData.options.filter(option => option.id!==optionId)
        setQuestionData({ ...questionData, options })
    }

    const setOptionValue = (newValue, id) => {
        const options = questionData.options.map(option => {
            if (option.id===id) {
                option = { ...option, value: newValue }
            }
            return option
        })
        setQuestionData({ ...questionData, options })
    }

    const options = () => questionData.options.map((option, index) => 
        <div className='flex alignItemsCenter' key={index}>
            <div className='marginRight-point5rem'>{index+1}.</div>
            <div className='modal-input marginBottom-point5rem'>
                <Input type={'text'} value={option.value} onChange={(value)=>setOptionValue(value, option.id)}/>
            </div>
            <div>
                <img src={closeIcon} className='close-icon-option'  onClick={()=>deleteOption(option.id)} />
            </div>
        </div>
    )

    const handleSelectChange = ({ id, value }, label) => {
        if (label==='Select Option Type') setOptionType({ ...optionType, id, value })
        else if (label==='Select Page') setPages({ ...pages, id, value })
    }
    
    const multiSelectInput = (inputData) => {
        return <MultiSelect
                    labelKey={'value'}
                    selectedOptions={inputData.id}
                    options={inputData.options}
                    onChange={(value) => handleSelectChange(value, inputData.label)}
                    placeholder={`${inputData.label}`}
                    checkbox_id="id"
                    disabledItems={[]}
                    key={1}
                />
    }

    const question = () => {
        return  <div className='question'>
                    <div className='flex alignItemsCenter marginBottom-2rem'>
                        <div className='modal-input'>
                            <Input label={questionData.label} type={'text'} value={questionData.title} onChange={setQuesTionTitle}/>
                        </div>
                        <div className='options-container'>{multiSelectInput(optionType)}</div>
                        <div className='options-container'>{multiSelectInput(pages)}</div>
                    </div>
                    {optionType.id && optionType.id!==1 && 
                        <>
                            {options()}
                            <div className="makeButton makeBondiBlueButton marginLeft-1rem " onClick={addOption}>+ Option</div>
                        </>
                    }
                </div>
    }

    const page = () => {
        return  <div className='flex justifyContentCenter page-container'>
                    <div className='page-title modal-input'>
                        <Input label={pageData.label} type={pageData.type} value={pageData.value} onChange={setPageDaTa}/>
                    </div>
                </div>
    }

    const modalData = () => {
        if (modalOf==='page') return page()
        else if (modalOf==='question') return question()
    }

    const actionButtons = () => (
        <div className='buttons-contaner'>
            <div className="makeButton makeFunGreenButton marginRight-point5rem"
                onClick={
                    modalOf==='page' && pageData.value ? () => { savePage(); toggLeModal() } 
                    : modalOf==='question' && questionData.title ? () => { saveQuestion(); toggLeModal() } : null
                }
            >
                Save
            </div>
            <div className="makeButton makeThunderBirdButton" onClick={toggLeModal}>Cancel</div>
        </div>
    )


    return ( 
        <div>
            <div className={`modal ${modalOf==='page' ? 'modal-page-height' : 'modal-question-height'}`}>
                {modalOf==='question' && !allPages.length ?
                    <div className='flex justifyContentCenter alert-text'>Please Add a Page before adding a new question.</div>
                : 
                    <>
                        {modalData()} 
                        {actionButtons()}
                    </>
                }
            </div>
            <div className={`modal-overlay`}
                onClick={toggLeModal}
            >
            </div>
        </div>
     );
}
 
export default Modal;