import React, { useState, useEffect } from 'react';
import './InputBox.css';

const Input = (props) => {
  const [state, updateState] = useState({
    isInputActive: false,
    value: ''
  });
  const { label, type, value, errorMessage } = props

  useEffect(() => {
    if (type === "date" || type === "time") {
      updateState({ ...state, isInputActive: true })
    }
  }, [])

  useEffect(() => {
    if (value) {
      updateState({ value: value, isInputActive: true })
    }
    else {
      if (type === "date" || type === "time") {
        updateState({ value:'',isInputActive: true })
      }
      else {
        updateState({ value: '', isInputActive: false })
      }
    }
  }, [value])

  const onChange = (e) => {
    updateState({ ...state, value: e.target.value })
    props.onChange(e.target.value, label) 
  }
  const onFloat = () => {
    if (!state.isInputActive) {
      updateState({ ...state, isInputActive: true })
    }
  }

  const floatOut = (event) => {
    if (state.isInputActive && !event.target.value && !(type === "date") && !(type === "time")) {
      updateState({ ...state, isInputActive: false })
    }
  }
  
  let textInput = React.createRef();
  return (
    <div id="inputContainer" className={`${state.isInputActive ? 'inputContainer active' : 'inputContainer'}${(errorMessage)?' error':''}`}>
      <span className="inputTitle" onClick={()=>{
        textInput.current.focus();
        updateState({...state,isInputActive:true})
      }}>{props.label}</span>
      <div className="mainInputContainer">
        <input ref={textInput} autoComplete="off" id="floatField" disabled={props.disabled ? props.disabled : false} 
          type={type} className="mainInput" value={state.value}  onFocus={onFloat} onBlur={floatOut} 
          onChange={onChange} id={Math.random()}/>
      </div>
      <div className="errorMessage">{errorMessage}</div>
    </div>
  )
}

export default Input;