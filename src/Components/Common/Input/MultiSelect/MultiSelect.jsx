import React, { useState, Fragment } from "react";
import './MultiSelect.css';

const MultipleSelection = (props) => {
    const [searchString, updateSearchString] = useState('');

    const updateSelectionList = (event, option) => {
        let new_selection = null;
        if (event.target.checked) {
            new_selection = [...props.selectedOptions, option];
        } else {
            new_selection = props.selectedOptions.filter((_selected_option) => _selected_option[props.checkbox_id] !== option[props.checkbox_id]);
        }
        if (new_selection) {
            props.onChange(new_selection);
        }
    };

    const handleSingleSelection = (option) => {
        updateSearchString('');
        props.onChange(option);
    };

    const checkSelect = () => {
        return props.multiple ? null :
            (props.selectedOptions && props.options) ?
                props.options.reduce(
                    (a, v) => v[props.checkbox_id] === props.selectedOptions ? v[props.labelKey] : a, null
                )
                : null
    };

    const updaTeSearchString = (value) => {
        updateSearchString(value)
        if (props.onType) props.onType(value)
    }

    return (
        <div>
            <div className="dropdown-check-list">
                <span className={`selected-label ${props.multiple ? 'color-black-label' : ''}`}>
                    {
                        props.multiple ?
                            props.selectedOptions && props.selectedOptions.length > 0 ?
                                props.selectedOptions.map(selected => selected[props.labelKey]).join(' | ') :
                                ''
                            : checkSelect() ? props.placeholder : ''
                    }
                </span>
                <input className={`anchor ${props.notValid ? 'input-box-error' : ''} ${checkSelect() ? 'color-black-placeholder' : ''}`} placeholder={
                    props.multiple ? props.placeholder : checkSelect() ? checkSelect() : props.placeholder
                } value={searchString} disabled={props.disabled}
                    onChange={(event) => updaTeSearchString(event.target.value)} />
                <ul className="options">
                    {
                        props.options && props.options.length > 0 ?
                            props.options.filter((filter_option) =>
                                filter_option[props.labelKey].toLowerCase().indexOf(searchString.toLowerCase()) !== -1
                            ).map((option) => (
                                <Fragment key={`${option[props.labelKey]}-${option[props.checkbox_id]}`}> {
                                    props.multiple ?
                                        <li>
                                            <label htmlFor={`checkbox${props.labelKey}${option[props.checkbox_id]}`}>
                                                {
                                                    props.disabledItems &&
                                                    props.disabledItems.reduce(
                                                        (a, v) => v[props.checkbox_id] === option[props.checkbox_id] ?
                                                            null : a,
                                                        <input type="checkbox"
                                                            id={`checkbox${props.labelKey}${option[props.checkbox_id]}`}
                                                            onChange={(event) => updateSelectionList(event, option)}
                                                            checked={
                                                                props.selectedOptions &&
                                                                props.selectedOptions.length > 0 &&
                                                                props.selectedOptions.reduce(
                                                                    (a, v) => v[props.checkbox_id] === option[props.checkbox_id] ? true : a, false
                                                                )
                                                            }
                                                        />)
                                                }
                                                <span className="margin_l_8">
                                                    {option[props.labelKey]}
                                                </span>
                                            </label>
                                        </li> :
                                        <li onClick={() => handleSingleSelection(option)}>
                                            <label
                                                className={option[props.checkbox_id] === props.selectedOptions ? 'single-selected-item' : ''}>
                                                <span className="margin_l_8">
                                                    {option[props.labelKey]}
                                                </span>
                                            </label>
                                        </li>
                                } </Fragment>
                            )) : null
                    }
                </ul>
            </div>
            {
                props.error &&
                <label className="multi-select-error">{props.error}</label>
            }
        </div>
    )
};

export default MultipleSelection;
