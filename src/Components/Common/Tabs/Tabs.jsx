import React from "react";
import './Tabs.css'
import closeIcon from '../../../Images/close.png'


const Tabs = (props) => {
    const { tabElements, setActiveTab, activeTabId, tabsType, closeTab } = props

    const setAcTiveTab = ({ value, id }) => setActiveTab(value, id)

    const cLoseTab = ({ value, id }) => closeTab('delete', value, id)

    const tabEleMents = () => {
        const elements = tabElements.map((element, index) => {
            const acTiveTabClass = activeTabId===element.id ? `${activeTabClass()}` : ''
            if (element.id) {
                return <div className='tab-container' key={index}>
                            <div className={`${acTiveTabClass} ${tabsTypeClass()}`} onClick={()=>setAcTiveTab(element)}>
                                    {element.value}
                            </div>
                            <img src={closeIcon} className='close-icon' onClick={()=>cLoseTab(element)} />
                        </div>


            }
        })
        return elements
    }

    const tabsTypeClass = () => {
        const tabsTypeClass = tabsType==='header' ? 'header-tab' : 'sub-header-tab'
        return tabsTypeClass
    }

    const activeTabClass = () => {
        const activeTabClass = tabsType==='header' ? 'active-header-tab' : 'active-sub-header-tab'
        return activeTabClass
    }


    return ( 
            <div className='tabs-container'>
                {tabEleMents()}
            </div>
     );
}


export default Tabs