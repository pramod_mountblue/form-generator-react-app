import React, { Suspense, lazy } from "react";
import { Route, Switch, BrowserRouter as Router } from "react-router-dom";

const Home = lazy(() => import("../Home/Home"));

const Routes = () => {
    return (
        <Suspense fallback={<div></div>}>
            <Router>
                <Switch>
                    <Route exact path="/" component={Home} />
                </Switch>
            </Router>
        </Suspense>
    );
};

export default Routes;
