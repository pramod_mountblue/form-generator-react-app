import React from 'react';
import Routes from './Components/Routes/Routes'
import "./Styles/GlobalCss.css";

function App() {
  return ( <Routes /> );
}

export default App;
